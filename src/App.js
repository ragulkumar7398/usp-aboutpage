import React from 'react'
import './App.css';
import About from './components/About/about';
import Banner from './components/Banner/banner';
import Course from './components/Course/course';
import Learn from './components/Learn/learn';
import Navbars from './components/Navbar/navbar';
import Purpose from './components/Purpose/purpose';
import Subscribe from './components/Subscribe/subscribe';
import Topic from './components/Topic/topic';


function App() {
  return (
    <div className="App">
      <Navbars />
      <Banner />
      <Course />
      <About />
      <Topic />
      <Learn />
      <Purpose />
      <Subscribe />
    </div>
  );
}

export default App;
