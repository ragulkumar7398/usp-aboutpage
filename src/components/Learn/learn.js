import React from 'react'
import { Container, Row , Col } from 'react-bootstrap'
import './learn.scss'
import learn from './img/learn.png'

function Learn() {
    return(
    <Container fluid className="learnContainer">

        <Row>
            <Col xl={8}>
                <img src={learn} className="bannerImage" alt="sideImage"/>
            </Col>
            <Col xl={4}>
                <p className="learnHeading"><b>Learning <br /> Outcomes</b></p>
                <hr  className="h_line" />

                <div className="learnBox">
                    <ul>
                        <li className="learnList">
                        Describe why it is important to teach Quality Physical Education in the Pacific;
                        </li>
                        <li className="learnList">
                        Describe how traditional games and dance influence movement culture in the Pacific and how they can be embedded in Quality Physical Education lessons; 
                        </li>
                        <li className="learnList">
                        Outline the historical and political contexts of health and physical education in the Pacific;  
                        </li>
                        <li className="learnList">
                        Discuss the practices and beliefs with colleagues to support your professional development; 
                        </li>
                        <li className="learnList">
                        Identify strategies to plan inclusive physical education lessons to cater for the needs of all learners;
                        </li>
                        <li className="learnList">
                        Demonstrate how to design a quality physical education lesson; and
                        </li>
                        <li className="learnList">
                        Develop strategies to support your learning and development after this MOOC. 
                        </li>
                    </ul>
                </div>

            </Col>
        </Row>

    </Container>
    )
}


export default Learn
