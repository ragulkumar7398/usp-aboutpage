import React from 'react'
import './navbar.scss'
import { Nav , Navbar, NavDropdown} from 'react-bootstrap'
import logo from './img/logo_usp.png'

function Navbars() {
  return(
<div className="navColor w-100">
    
    <Navbar collapseOnSelect expand="lg"  className="navbar-primary w-80"> 
  <Navbar.Brand href="#home"><img src={logo} alt="Logo" / ></Navbar.Brand>
  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav">
    <Nav className="nav">
      <Nav.Link href="#features" className="listColor">About</Nav.Link>
      <NavDropdown title="Courses" className="listColor" id="collasible-nav-dropdown">
      </NavDropdown>
    </Nav>
    <Nav className="ml-auto">
      <Nav.Link href="#deets">Sign in</Nav.Link>
      <button className="btn btn-primarys"><b>Register</b></button>
    </Nav>
  </Navbar.Collapse>
</Navbar>
</div>

  )
}


export default Navbars;


