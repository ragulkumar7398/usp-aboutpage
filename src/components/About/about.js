import React from 'react'
import { Container} from 'react-bootstrap';
import './about.scss'

function About() {
    return(
        <Container className="aboutContainer"> 
        <div className="aboutBox">
           <p className="aboutHead"><b>Learn How to Create Inclusive Physical Education Lessons</b></p>
           <p className="aboutPara">This course is tailored to the Pacific experience of human movement and culture to explore traditional sports and games. Recognising the diversity within classrooms across the Pacific, this course also includes specific content on inclusive physical education with tips for teachers on adapting and modifying activities to suit the learning needs of every student.</p> 
           <p className="aboutPara"> Participants will have access to professional mentors from the Pacific Islands with a range of experience across physical education, physical activity and sport. Participants will also be able to connect and interact with each other through discussion and community forum for support and celebrate progress.</p>           
        </div>
        </Container>
    )
}


export default About;