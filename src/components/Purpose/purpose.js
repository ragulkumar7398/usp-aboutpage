import React from 'react'
import { Container , Row , Col } from 'react-bootstrap'
import './purpose.scss'
import purpose from './img/purpose.png'


function Purpose() {
    return(
        <Container fluid className="purposeContainer">
            <Row>
                <Col xl={5}>

                 <p className="purposeHeading"><b>This Course <br />Is For</b></p>
                 <hr className="v_line" />
                    <div>
                        <p className="purposePara">The course is designed for anyone with an interest in learning more about how to deliver quality physical education in the pacific including</p>
                        <ul className="purposeListhead">
                            <li className="purposeList">Pre-service and in-service teachers;</li>
                            <li className="purposeList">Classroom teachers with no background in physical education; </li>
                            <li className="purposeList">School Leavers;</li>
                            <li className="purposeList">Community sport coaches, administrators and volunteers; and</li>
                            <li className="purposeList">People interested in all levels of education (early childhood, primary, secondary and special and inclusive education). </li>
                        </ul>
                    </div>
                </Col>
                <Col xl={7}>
                 <img src={purpose} className="purposeBanner" alt="aboutBanner"/>
                </Col>
            </Row>
        </Container>
    )
}

export default Purpose
