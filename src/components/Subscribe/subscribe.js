import React from 'react'
import './subscribe.scss'
import {InputGroup , FormControl } from 'react-bootstrap'

function Subscribe() {
    return(
        <div className="subscribeContainer">
            <p className="text-center subscribePara"><b>Shaping Your Pacific Future</b></p>
            <p className="subPara">Stay ahead of the pack and sign up to our newsletter to keep updated with course recommendations and promotions.</p>
            <div className="subscribeInput">
                <InputGroup size="lg">
                <FormControl
                placeholder="Enter your Email"
                aria-label="Recipient's username"
                aria-describedby="basic-addon2"
                className="inputBox"
                />
                <InputGroup.Append className="subBtnprimary">
                <InputGroup.Text id="basic-addon2" className="subBtn"><b>Subscribe</b></InputGroup.Text>
                </InputGroup.Append>
            </InputGroup>
            </div>
        </div>
    )
}

export default Subscribe;