import React from 'react';
import { Container } from 'react-bootstrap';
import './topic.scss';

function Topic() {
    return(
        <Container className="topicContainer">
            <div className="topicInnerbox">
            <p className="topicHeading"><b>Key Topics This Course</b></p>
            <div className="topicListcontainer">
                <ul>
                    <li className="topicList">
                    <b>Week 1:</b> Introducing Quality Physical Education
                    </li>
                    <li className="topicList">
                    <b>Week 2:</b> Traditional Games and Sports in the Pacific
                    </li>
                    <li className="topicList">
                    <b>Week 3:</b> Understanding Health and Physical Activity in the Pacific
                    </li>
                    <li className="topicList">
                    <b>Week 4:</b> Inclusive Physical Education
                    </li>
                    <li className="topicList">
                    <b>Week 5:</b> How to be a Reflective Physical Education Teacher
                    </li>
                    <li className="topicList">
                    <b>Week 6:</b> Designing a Quality Physical Education Lesson
                    </li>
                </ul>
            </div>
            </div>
        </Container>
    )
}

export default Topic;