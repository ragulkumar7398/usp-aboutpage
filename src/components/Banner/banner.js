import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import './banner.scss'
import banner from './img/banner.jpg'
import logoone from './img/logo1.png'
import logotwo from './img/logo2.png'


function Banner() {
    return(
        <div className="w-100 h-100">
            <Row>
                <Col xl={5} className="topBox">
                    <Container className="w-80">
                        <Row className="myRow">
                            <Col sm={3} className="col-3"><img src={logoone} className="logo" alt="logo1"/></Col>
                            <Col sm={3} className="col-3"><img src={logotwo} className="logo" alt="logo2"/></Col>
                        </Row>
                        <h1 className="mar-5">
                        QUALITY PHYSICAL EDUCATION IN THE PACIFIC
                        </h1>
                        <p className="bannerPara mar-5">
                        Learn how to provide inclusive, flexible and interactive physical education lessons for all types of learners.
                        </p>
                        <button className="btn btn-infos mar-5"><b>Ready to Enroll</b></button>
                    </Container>
                </Col>
                <Col xl={7}><img src={banner} className="headerBanner w-100" alt="banner"/></Col>
            </Row>
        </div>
    )
}


export default Banner