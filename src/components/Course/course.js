import React from 'react'
import { Col, Container, Row } from 'react-bootstrap';

import './course.scss'
import logoone from './img/icon1.png'
import logotwo from './img/icon2.png'
import logothree from './img/icon3.png'
import logofour from './img/icon4.png'
import logofive from './img/icon7.png'
import logosix from './img/icon6.png'



function Course() {
    return(
        <Container fluid className="bg"> 
            <Container className="pad-3">
                <Row>
                    <Col sm={3}>
                        <div className="myCardone w-100">
                        <div className="innerCard">
                        <img src={logoone} className="icon" alt=""/>
                        <div className="alignPara">
                        <h6 className="mar-10 ">Duration</h6>
                        <p className="cardPara contentCenterone">6 Weeks</p>
                        </div>
                        </div>
                        </div>
                    </Col>
                    <Col sm={3}><div className="myCard w-100">
                    <div className="innerCard">
                        <img src={logotwo} className="icon" alt=""/>
                        <div className="alignParatwo">
                        <h6 className="mar-10">Time Commitment</h6>
                        <p className="cardPara contentCentertwo">4hours / Weeks</p>
                        </div>
                        </div>
                       
                        </div></Col>
                    <Col sm={3}><div className="myCardone w-100">
                    <div className="innerCard">
                        <img src={logothree} className="icon" alt=""/>
                        <div className="alignParathree">
                        <h6 className="mar-10">Assessment</h6>
                        <p className="cardPara contentCenter">Yes</p>
                        </div>
                        </div>
                        </div>
                    </Col>
                    <Col sm={3}><div className="myCard w-100">

                    <div className="innerCard">
                        <img src={logofour} className="icon" alt=""/>
                        <div className="alignParafour">
                        <h6 className="mar-10">Learn</h6>
                        <p className="cardPara">For Free</p>
                        </div>
                        </div>
                        </div></Col>
                </Row>
                <div className="cardContainer">
                <Row > 
                    <Col sm={6}>
                        <div className="myCardone w-100">
                            <Container >
                            <Row className="alignBox">
                                <Col xl={3}><img src={logofive} className="icons" alt=""/></Col>
                                <Col xl={9}>
                                    <h6 className="secondCardhead">start dates</h6>
                                    <p className="cardParas">9 November 2020</p>
                                </Col>
                            </Row>
                            </Container>
                        </div>
                    </Col>
                    <Col sm={6}>
                        <div className="myCard w-100">
                        <Container >
                            <Row className="alignBox">
                                <Col xl={3}><img src={logosix} className="icons" alt=""/></Col>
                                <Col xl={9}>
                                    <h6 className="secondCardheadtwo">Digital credentials</h6>
                                    <p className="cardParastwo">Certificate of Completion</p>
                                </Col>
                            </Row>
                            </Container>
                            
                        </div>
                    </Col>
                </Row>
                </div>
            </Container>
        </Container>
    )
}

export default Course;